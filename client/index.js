import {
  Object,
  everyTick,
  isKeyDown,
  onServer,
  Player,
  on,
  off,
  emitServer,
  log,
} from 'alt-client';
import { Utils, clearEveryTick } from 'alt-shared';
import {
  freezeEntityPosition,
  createCam,
  hardAttachCamToEntity,
  renderScriptCams,
  getGameplayCamRot,
  setEntityRotation,
  getEntityForwardVector,
  setEntityCoordsNoOffset,
  addExplosion,
  destroyCam,
  disableControlAction,
} from 'natives';

const DRONE_SPEED = 0.1;

onServer('createDrone', async (droneId) => {
  const drone = Object.getByID(droneId);
  if (!drone.isSpawned) await Utils.waitFor(() => drone.isSpawned);

  freezeEntityPosition(Player.local.scriptID, true);

  const cam = createCam('DEFAULT_SCRIPTED_CAMERA', true);
  hardAttachCamToEntity(cam, drone, 0.0, 0.0, 0.0, 0.0, 0.15, -0.1, true);
  renderScriptCams(1, 0, 0, 0, 0, 0);

  let rotTick = everyTick(() => {
    const rot = getGameplayCamRot(0);
    setEntityRotation(drone, rot.x, rot.y, rot.z, 0, true);
    disableControlAction(0, 200, true);
  });

  on('keydown', buttonHandle);

  function buttonHandle(key) {
    const keys = [1, 70, 87, 83, 68, 65];
    if (!keys.includes(key)) return;

    if (key === 1) {
      const pos = drone.pos.sub(0, 0, 1);
      emitServer('createBomb', pos);
      return;
    }

    if (key === 70) {
      renderScriptCams(0, 0, 0, 0, 0, 0);
      destroyCam(cam, true);
      off('keydown', buttonHandle);
      emitServer('destroyDrone', drone);
      freezeEntityPosition(Player.local.scriptID, false);
      clearEveryTick(rotTick);
      return;
    }

    const tick = everyTick(() => {
      const droneVector = getEntityForwardVector(drone).mul(DRONE_SPEED);

      let newPos = drone.pos;
      if (key === 87) {
        newPos = newPos.add(droneVector);
      } else if (key === 83) {
        newPos = newPos.sub(droneVector);
      } else if (key === 65) {
        newPos = newPos.sub(droneVector.cross(0, 0, 1));
      } else if (key === 68) {
        newPos = newPos.add(droneVector.cross(0, 0, 1));
      }

      setEntityCoordsNoOffset(
        drone,
        newPos.x,
        newPos.y,
        newPos.z,
        false,
        false,
        false
      );

      const keyDown = keys.some((key) => isKeyDown(key));

      if (!keyDown) {
        clearEveryTick(tick);
      }
    });
  }
});

onServer('bombDetonate', (pos) => {
  addExplosion(pos.x, pos.y, pos.z, 0, 5, true, false, 1, false);
});
