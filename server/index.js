import { on, onClient, setTimeout, Object } from 'alt-server';
import { Vector3 } from 'alt-shared';
import * as chat from 'alt:chat';

const spawnPos = new Vector3(813, -279, 66);

on('playerConnect', (player) => {
  player.model = 'mp_m_freemode_01';
  player.spawn(spawnPos);
});

chat.registerCmd('drone', (player) => {
  chat.send(
    player,
    'Керування дроном: WASD, cкинути бомбу: LMB, вийти з дрону: F'
  );

  const drone = new Object(
    'reh_prop_reh_drone_02a',
    player.pos.add(0, 0, 5),
    player.rot,
    true,
    false
  );
  drone.frozen = true;
  player.emit('createDrone', drone.id);
});

onClient('createBomb', (player, pos) => {
  const bomb = new Object('v_res_fa_shoebox2', pos, Vector3.zero, true, false);

  setTimeout(() => {
    const pos = bomb.pos;
    player.emit('bombDetonate', pos);
    bomb.destroy();
  }, 5000);
});

onClient('destroyDrone', (_, drone) => {
  drone.destroy();
});
